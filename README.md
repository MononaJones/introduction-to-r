# Introduction to R #

Files used at a Code Chix Madison presentation on January 14, 2016

[Download PDF of the Presentation](https://bitbucket.org/MononaJones/introduction-to-r/downloads/Introduction to the R programming language.pdf)

[Download PDF of Other Resource](https://bitbucket.org/MononaJones/introduction-to-r/downloads/R Resources.pdf)